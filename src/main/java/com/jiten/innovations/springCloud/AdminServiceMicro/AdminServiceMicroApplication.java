package com.jiten.innovations.springCloud.AdminServiceMicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminServiceMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminServiceMicroApplication.class, args);
	}

}
